from bson.objectid import ObjectId

class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username, emotion):
        self.conn.insert({'post':post, 'username': username, 'emotion': emotion})

    def updatePost(self, post, username, emotion, id):
    	self.conn.update({"_id":ObjectId(id)}, {"$set":{'post':post, 'username': username, 'emotion': emotion}}) 

    def deletePost(self, id):
    	self.conn.delete_one({"_id":ObjectId(id)}) 