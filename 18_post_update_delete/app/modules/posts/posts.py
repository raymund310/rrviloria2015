from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts, date='October 20 2015')

@mod.route('/', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    emotion = request.form['emotion']
    g.postsdb.createPost(new_post, session['username'], emotion)
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/update', methods=['POST'])
def update_post():
    u_post = request.form['post_update']
    u_emotion = request.form['emotion_update']
    id = request.form['u_id']
    g.postsdb.updatePost(u_post, session['username'], u_emotion, id)
    flash('Post updated!', 'create_post_success')
    return redirect(url_for('.post_list'))


@mod.route('/delete', methods=['POST'])
def delete_post():
    id = request.form['d_id']
    g.postsdb.deletePost(id)
    flash('Post deleted!', 'create_post_success')
    return redirect(url_for('.post_list'))