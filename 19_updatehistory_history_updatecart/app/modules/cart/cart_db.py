from bson.objectid import ObjectId

class CartDB:
    def __init__(self, conn):
        self.conn = conn

    def getCart(self):
        return self.conn.find()

    def findItem(self, i_id):
        return self.conn.find({'i_id': ObjectId(i_id)})

    def createCart(self, name, price, i_id):
        self.conn.insert({'name':name, 'price': price, 'i_id': i_id})

    def updateCart(self, name, price, i_id):
    	self.conn.update({"i_id":ObjectId(i_id)}, {"$set":{'name':name, 'price': price}}) 

    def deleteCart(self):
    	self.conn.delete_many({ }) 