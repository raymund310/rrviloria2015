from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('cart', __name__)

@mod.route('/', methods=['GET', 'POST'])
def cart_list():
    invents = g.cartdb.getCart()
    #return render_template('default/index.html', invents=invents)

@mod.route('/add', methods=['POST'])
def add_cart():
    item_name = request.form['name']
    price = request.form['price']
    g.cartdb.createCart(item_name, price)
    flash('New Inventory Added!', 'add_item_success')
    #return redirect(url_for('.inventory_list'))


@mod.route('/delete', methods=['POST'])
def delete_invent():
    g.cartdb.deleteCart(id)
    #flash('Post deleted!', 'create_post_success')
    #return redirect(url_for('.inventory_list'))