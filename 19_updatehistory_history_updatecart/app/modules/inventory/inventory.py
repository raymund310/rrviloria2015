from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('inventories', __name__)

@mod.route('/', methods=['GET', 'POST'])
def inventory_list():
    invents = g.salesdb.getItems()
    carts = g.cartdb.getCart()
    histories = g.historydb.getHistory()
    histories.sort("_id", -1)
    if request.method == "POST":
        q = [x for x in request.form.getlist('quantity')]
        for x in xrange(0, len(q)):
            if int(q[x]) != 0:
                cart = g.cartdb.findItem(invents[x]["_id"])
                new_price = int(invents[x]["price"]) * int(q[x])
                x_name = invents[x]["name"]
                x_id = invents[x]["_id"]
                if cart.count() <= 0:
                    g.cartdb.createCart(x_name, new_price, x_id)    
                else:
                    g.cartdb.updateCart(x_name, new_price + int(cart[0]["price"]), x_id)

    return render_template('default/index.html', invents=invents, carts=carts, histories=histories)

@mod.route('/add', methods=['POST'])
def add_invent():
    item_name = request.form['name']
    price = request.form['price']
    g.salesdb.createItem(item_name, price)
    flash('New Inventory Added!', 'add_item_success')
    return redirect(url_for('.inventory_list'))


@mod.route('/delete', methods=['POST'])
def delete_purchase():
    carts = g.cartdb.getCart()
    [g.historydb.appendHistory(x["name"], x["price"]) for x in carts]
    g.cartdb.deleteCart()
    return redirect(url_for('.inventory_list'))

@mod.route('/update', methods=['POST'])
def update_invent():
    item_name = request.form['name']
    price = request.form['price']
    u_id = request.form['u_id']
    g.salesdb.updateItem(item_name, price, u_id)
    return redirect(url_for('.inventory_list'))