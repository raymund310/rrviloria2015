from bson.objectid import ObjectId

class HistoryDB:
    def __init__(self, conn):
        self.conn = conn

    def getHistory(self):
        return self.conn.find()

    def appendHistory(self, name, price):
        self.conn.insert({'name':name, 'price': price})

    #def updatePost(self, post, username, emotion, id):
    	#self.conn.update({"_id":ObjectId(id)}, {"$set":{'post':post, 'username': username, 'emotion': emotion}}) 

    def deleteHistory(self):
        self.conn.delete_many({ }) 