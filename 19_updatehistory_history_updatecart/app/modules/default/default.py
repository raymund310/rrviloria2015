from flask import Blueprint
from flask import render_template
from flask import redirect
from flask import session
from flask import request
from flask import flash
from flask import url_for
from flask import g
from functools import wraps

mod = Blueprint('default', __name__)

@mod.route('/')
def index():
    return redirect('/inventory/')