from bson.objectid import ObjectId

class InventoryDB:
    def __init__(self, conn):
        self.conn = conn

    def getItems(self):
        return self.conn.find()

    def getItem(self, id):
        return self.conn.find({"_id" : ObjectId(id)})

    def createItem(self, name, price):
        self.conn.insert({'name':name, 'price': price})

    def updateItem(self, name, price, u_id):
    	self.conn.update({"_id":ObjectId(u_id)}, {"$set":{'name':name, 'price': price}}) 

    def deleteItems(self):
    	self.conn.delete_many({ }) 