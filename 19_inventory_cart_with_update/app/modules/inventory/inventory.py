from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('inventories', __name__)

@mod.route('/', methods=['GET', 'POST'])
def inventory_list():
    invents = g.salesdb.getItems()
    print invents[0]
    carts = g.cartdb.getCart()
    if request.method == "POST":
        q = [x for x in request.form.getlist('quantity')]
        for x in xrange(0, len(q)):
            if int(q[x]) != 0:
                g.cartdb.createCart(invents[x]["name"], int(invents[x]["price"]) * int(q[x]))

    return render_template('default/index.html', invents=invents, carts=carts)

@mod.route('/add', methods=['POST'])
def add_invent():
    item_name = request.form['name']
    price = request.form['price']
    g.salesdb.createItem(item_name, price)
    flash('New Inventory Added!', 'add_item_success')
    return redirect(url_for('.inventory_list'))


@mod.route('/delete', methods=['POST'])
def delete_purchase():
    g.cartdb.deleteCart()
    #flash('Post deleted!', 'create_post_success')
    return redirect(url_for('.inventory_list'))

@mod.route('/update', methods=['POST'])
def update_invent():
    item_name = request.form['name']
    price = request.form['price']
    u_id = request.form['u_id']
    g.salesdb.updateItem(item_name, price, u_id)
    return redirect(url_for('.inventory_list'))