from bson.objectid import ObjectId

class CartDB:
    def __init__(self, conn):
        self.conn = conn

    def getCart(self):
        return self.conn.find()

    def createCart(self, name, price):
        self.conn.insert({'name':name, 'price': price})

    #def updatePost(self, post, username, emotion, id):
    	#self.conn.update({"_id":ObjectId(id)}, {"$set":{'post':post, 'username': username, 'emotion': emotion}}) 

    def deleteCart(self):
    	self.conn.delete_many({ }) 