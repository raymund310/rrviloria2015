from flask import Flask
from flask import g

from modules import inventory
from modules import cart
from modules import default
from modules import util

from pymongo import MongoClient

app = Flask(__name__)

app.session_interface = util.MongoSessionInterface(db='sessions')

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.salesdb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.salesdb = inventory.InventoryDB(conn=mainDb.inventory)
    g.cartdb = cart.CartDB(conn=mainDb.cart)


app.register_blueprint(default.mod)
#app.register_blueprint(cart.mod, url_prefix="/cart")
app.register_blueprint(inventory.mod, url_prefix="/inventory")