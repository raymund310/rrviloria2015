class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username, emotion):
        self.conn.insert({'post':post, 'username': username, 'emotion': emotion})