var Post = React.createClass({
  render: function() {
    var style = {
      backgroundColor: this.props.bgcolor
    }
    return (
      <div style={style}>
        {this.props.comment.i_name} {this.props.comment.price}
        <input type="text" name="quantity"/>
      </div>
    );
  }
});

var PostList = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var i_name = form.i_name.value.trim();
    var price = form.price.value.trim();

    this.props.onCommentSubmit({i_name: i_name, price: price});
    form.i_name.value = '';
    form.price.value = '';
    return;
  },
  render: function() {
    var postNodes = this.props.data.map(function (comment, index) {
      return (
        <Post key={index} comment={comment}/>
      );
    });
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
        {postNodes}
        <input type="submit" value="Add to cart"/>
        </form>
      </div>
    );
  }
});

var PostForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var i_name = form.i_name.value.trim();
    var price = form.price.value.trim();

    this.props.onCommentSubmit({i_name: i_name, price: price});
    form.i_name.value = '';
    form.price.value = '';
    return;
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Item name" name="i_name"/>
        <input type="text" placeholder="Price" name="price"/>
        <input type="submit" value="Add Item" />
      </form>
    );
  }
});

var DefaultURLMixin = {
  getDefaultProps: function () {
      return {url: "/getsampledata"};
  }
};

var DefaultPollMixin = {
  getDefaultProps: function () {
      return {pollInterval: 2000};
  }
};

var PostBox = React.createClass({
  mixins: [DefaultURLMixin, DefaultPollMixin],
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
      <div>
        <h1>Posts</h1>
        <PostList data={this.state.data} onCommentSubmit={this.handleCommentSubmit } />
        <PostForm onCommentSubmit={this.handleCommentSubmit}/>
      </div>
    );
  }
});